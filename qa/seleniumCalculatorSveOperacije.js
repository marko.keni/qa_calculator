const { Builder, By, Key } = require('selenium-webdriver')
const assert = require('assert')

const calculator = async() => {
    let driver = new Builder().forBrowser('chrome').build()
    await driver.get('file://E:/Desktop/qa/mojselenium/calculator/calculator.html')
    await driver.manage().window().maximize()
   
    await driver.sleep(500)
    let sedam = await driver.findElement(By.xpath("/html/body/div/table/tbody/tr[4]/td[1]/input"));
    await sedam.click()
    await driver.sleep(1000)
    let plus = await driver.findElement(By.xpath("/html/body/div/table/tbody/tr[4]/td[4]/input"));
    await plus.click()
    await driver.sleep(1000)
    let osam = await driver.findElement(By.xpath("/html/body/div/table/tbody/tr[4]/td[2]/input"));
    await osam.click()
    await driver.sleep(1000)
    let jednako1 = await driver.findElement(By.xpath("/html/body/div/table/tbody/tr[5]/td[3]/input"));
    await jednako1.click() 
    await driver.sleep(1000)
    let rezultat1 = await driver.findElement(By.xpath("/html/body/div/table/tbody/tr[1]/td[1]/input")).getAttribute("value")
    console.log(rezultat1)
    assert.equal(rezultat1, "15")
    let c1 = await driver.findElement(By.xpath("/html/body/div/table/tbody/tr[1]/td[2]/input"));
    await c1.click()
    await driver.sleep(1000)
 
    let devet = await driver.findElement(By.xpath("/html/body/div/table/tbody/tr[4]/td[3]/input"));
    await devet.click()
    await driver.sleep(1000)
    let minus = await driver.findElement(By.xpath("/html/body/div/table/tbody/tr[3]/td[4]/input"));
    await minus.click()
    await driver.sleep(1000)
    let tri = await driver.findElement(By.xpath("/html/body/div/table/tbody/tr[2]/td[3]/input"));
    await tri.click()
    await driver.sleep(1000)
    let jednako2 = await driver.findElement(By.xpath("/html/body/div/table/tbody/tr[5]/td[3]/input"));
    await jednako2.click()
    await driver.sleep(1000) 
    let rezultat2 = await driver.findElement(By.xpath("/html/body/div/table/tbody/tr[1]/td[1]/input")).getAttribute("value")
    console.log(rezultat2)
    assert.equal(rezultat2, "6")
    let c2 = await driver.findElement(By.xpath("/html/body/div/table/tbody/tr[1]/td[2]/input"));
    await c2.click()
    await driver.sleep(1000)

    let nula = await driver.findElement(By.xpath("/html/body/div/table/tbody/tr[5]/td[2]/input"));
    await nula.click()
    await driver.sleep(1000)
    let zarez = await driver.findElement(By.xpath("/html/body/div/table/tbody/tr[5]/td[1]/input"));
    await zarez.click()
    await driver.sleep(1000)
    let pet = await driver.findElement(By.xpath("/html/body/div/table/tbody/tr[3]/td[2]/input"));
    await pet.click()
    await driver.sleep(1000)
    let puta = await driver.findElement(By.xpath("/html/body/div/table/tbody/tr[5]/td[4]/input"));
    await puta.click()
    await driver.sleep(1000)
    let dva = await driver.findElement(By.xpath("/html/body/div/table/tbody/tr[2]/td[2]/input"));
    await dva.click()
    await driver.sleep(1000)
    let jednako3 = await driver.findElement(By.xpath("/html/body/div/table/tbody/tr[5]/td[3]/input"));
    await jednako3.click() 
    await driver.sleep(1000)
    let rezultat3 = await driver.findElement(By.xpath("/html/body/div/table/tbody/tr[1]/td[1]/input")).getAttribute("value")
    console.log(rezultat3)
    assert.equal(rezultat3, "1")
    let c3 = await driver.findElement(By.xpath("/html/body/div/table/tbody/tr[1]/td[2]/input"));
    await c3.click()
    await driver.sleep(1000)

    let jedan = await driver.findElement(By.xpath("/html/body/div/table/tbody/tr[2]/td[1]/input"));
    await jedan.click()
    await driver.sleep(1000)
    let sest = await driver.findElement(By.xpath("/html/body/div/table/tbody/tr[3]/td[3]/input"));
    await sest.click()
    await driver.sleep(1000)
    let podeljeno = await driver.findElement(By.xpath("/html/body/div/table/tbody/tr[2]/td[4]/input"));
    await podeljeno.click()
    await driver.sleep(1000)
    let cetiri = await driver.findElement(By.xpath("/html/body/div/table/tbody/tr[3]/td[1]/input"));
    await cetiri.click()
    await driver.sleep(1000)
    let jednako4 = await driver.findElement(By.xpath("/html/body/div/table/tbody/tr[5]/td[3]/input"));
    await jednako4.click() 
    await driver.sleep(1000)
    let rezultat4 = await driver.findElement(By.xpath("/html/body/div/table/tbody/tr[1]/td[1]/input")).getAttribute("value")
    console.log(rezultat4)
    assert.equal(rezultat4, "4")
    let c4 = await driver.findElement(By.xpath("/html/body/div/table/tbody/tr[1]/td[2]/input"));
    await c4.click()
    await driver.sleep(1000)

   
    driver.quit()

}

calculator()